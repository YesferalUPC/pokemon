package com.example.yesferal.yeslist.util;

import android.view.View;

public interface ListItemClickListener {
    void onRetryClick(View view, int position);
}
