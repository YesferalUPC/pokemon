package com.example.yesferal.yeslist.ui;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.example.yesferal.yeslist.repository.NetworkState;
import com.example.yesferal.yeslist.repository.inMemory.byitem.ItemKeyedPokemonDataSource;
import com.example.yesferal.yeslist.repository.inMemory.byitem.PokemonApiPokemonDataSourceFactory;
import com.example.yesferal.yeslist.vo.Pokemon;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class PokemonViewModel extends ViewModel {

    public LiveData<PagedList<Pokemon>> pokemonList;
    public LiveData<NetworkState> networkState;
    Executor executor;
    LiveData<ItemKeyedPokemonDataSource> tDataSource;

    public PokemonViewModel() {
        executor = Executors.newFixedThreadPool(5);
        PokemonApiPokemonDataSourceFactory pokemonApiPokemonDataSourceFactory = new PokemonApiPokemonDataSourceFactory(executor);

        tDataSource = pokemonApiPokemonDataSourceFactory.getMutableLiveData();

        networkState = Transformations.switchMap(pokemonApiPokemonDataSourceFactory.getMutableLiveData(), dataSource -> {
            return dataSource.getNetworkState();
        });

        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(10)
                        .setPageSize(20).build();

        pokemonList = (new LivePagedListBuilder(pokemonApiPokemonDataSourceFactory, pagedListConfig))
                .setBackgroundThreadExecutor(executor)
                .build();
    }
}
