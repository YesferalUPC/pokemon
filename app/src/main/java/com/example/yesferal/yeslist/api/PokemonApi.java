package com.example.yesferal.yeslist.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PokemonApi {

    private static String POKEMON_URL = "https://pokeapi.co/api/v2/";

    public static PokemonService createPokemonService() {
        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(POKEMON_URL);

        return builder.build().create(PokemonService.class);
    }
}
