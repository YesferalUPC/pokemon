package com.example.yesferal.yeslist;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.yesferal.yeslist.ui.PokemonAdapter;
import com.example.yesferal.yeslist.ui.PokemonViewModel;
import com.example.yesferal.yeslist.util.ListItemClickListener;

public class MainActivity extends AppCompatActivity implements ListItemClickListener {

    private PokemonViewModel pokemonViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView pokemonRecyclerView = findViewById(R.id.pokemonRecyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        pokemonRecyclerView.setLayoutManager(llm);

        pokemonViewModel = ViewModelProviders
                .of(this)
                .get(PokemonViewModel.class);

        final PokemonAdapter pokemonAdapter = new PokemonAdapter(this);

        pokemonViewModel.pokemonList.observe(this, pagedList -> {
            pokemonAdapter.submitList(pagedList); //setList(pagedList);
        });

        pokemonViewModel.networkState.observe(this, networkState -> {
            pokemonAdapter.setNetworkState(networkState);
            Log.d("MAIN_ACTIVITY", "Network State Change");
        });

        pokemonRecyclerView.setAdapter(pokemonAdapter);

    }

    @Override
    public void onRetryClick(View view, int position) {
        Log.d("YESFERAL", "Position " + position);
    }
}
