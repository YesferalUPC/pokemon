package com.example.yesferal.yeslist.repository.inMemory.byitem;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import java.util.concurrent.Executor;

public class PokemonApiPokemonDataSourceFactory implements DataSource.Factory{

    MutableLiveData<ItemKeyedPokemonDataSource> mutableLiveData;
    ItemKeyedPokemonDataSource itemKeyedPokemonDataSource;
    Executor executor;

    public PokemonApiPokemonDataSourceFactory(Executor executor) {
        this.mutableLiveData = new MutableLiveData<ItemKeyedPokemonDataSource>();
        this.executor = executor;
    }

    @Override
    public DataSource create() {
        itemKeyedPokemonDataSource = new ItemKeyedPokemonDataSource(executor);
        mutableLiveData.postValue(itemKeyedPokemonDataSource);
        return itemKeyedPokemonDataSource;
    }

    public MutableLiveData<ItemKeyedPokemonDataSource> getMutableLiveData() {
        return mutableLiveData;
    }

}
