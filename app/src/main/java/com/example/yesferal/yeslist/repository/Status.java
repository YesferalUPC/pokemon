package com.example.yesferal.yeslist.repository;

public enum Status{
    RUNNING,
    SUCCESS,
    FAILED
}