package com.example.yesferal.yeslist.api;

import com.example.yesferal.yeslist.vo.Pokemon;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PokemonService {
    @GET("pokemon/1")
    Call<List<Pokemon>> getPokemons(@Query("since") long since, @Query("per_page") int perPage);
}
