package com.example.yesferal.yeslist.ui;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.yesferal.yeslist.R;
import com.example.yesferal.yeslist.repository.NetworkState;
import com.example.yesferal.yeslist.repository.Status;
import com.example.yesferal.yeslist.util.ListItemClickListener;
import com.example.yesferal.yeslist.vo.Pokemon;

enum Tipo{
    pokemon,
    network
}

public class PokemonAdapter extends PagedListAdapter<Pokemon, RecyclerView.ViewHolder> {

    private static final int TIPO_POKEMON = 1;
    private static final int TIPO_NETWORK = 2;


    private NetworkState networkState;
    private ListItemClickListener itemClickListener;

    public PokemonAdapter(ListItemClickListener itemClickListener) {
        super(Pokemon.DIFF_CALLBACK);
        this.itemClickListener = itemClickListener;
    }


    // 2. DECIDE QUE CARD VA A MOSTRAR DEPENDIENDO DEL TIPO DE OBJETO
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;

        // LO NORMAL Q DEBERIA MOSTRAR
        if(viewType == TIPO_POKEMON) {
            //if (viewType == R.layout.item_pokemon_list) {
            view = layoutInflater.inflate(R.layout.item_pokemon_list, parent, false);
            return new PokemonItemViewHolder(view);


        } else if(viewType == TIPO_NETWORK){// LO Q MOSTRARA SI ES EL ULTIMO ELEMENTO
        //} else if (viewType == R.layout.network_state_item) {
            view = layoutInflater.inflate(R.layout.network_state_item, parent, false);
            return new NetworkStateItemViewHolder(view, itemClickListener);
        } else {
            throw new IllegalArgumentException("unknown view type");
        }
    }

    // OBJETO QUIERE MOSTRAR SU DATA
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TIPO_POKEMON://R.layout.item_pokemon_list:
                ((PokemonItemViewHolder) holder).bindTo(getItem(position));
                break;
            case TIPO_NETWORK://R.layout.network_state_item:
                ((NetworkStateItemViewHolder) holder).bindView(networkState);
                break;
        }
    }

    private boolean hasExtraRow() {
        if (networkState != null && networkState != NetworkState.LOADED) {
            return true;
        } else {
            return false;
        }
    }


    // 1.
    // OBJETO: QUE VISTA DEBO USAR ANDROID ?
    // ANDROID: QUE TIPO ERES ?
    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount()/*list.size()*/ - 1) {
            return TIPO_NETWORK;
            //return R.layout.network_state_item;
        } else {
            return TIPO_POKEMON;
            //return R.layout.item_pokemon_list;
        }
    }


    // COPY PASTE BRUTAL
    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }

    static class PokemonItemViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView, idTextView;

        public PokemonItemViewHolder(View itemView) {
            super(itemView);
            idTextView = itemView.findViewById(R.id.idTextView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
        }

        public void bindTo(final Pokemon pokemon) {
            nameTextView.setText(pokemon.getName());
            nameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("YESFERAL", pokemon.getName());
                }
            });
            idTextView.setText(pokemon.getUrl());
        }
    }

    // Copy & Paste MALDITO
    static class NetworkStateItemViewHolder extends RecyclerView.ViewHolder {

        private final ProgressBar progressBar;
        private final TextView errorMsg;
        private Button button;

        public NetworkStateItemViewHolder(View itemView, final ListItemClickListener listItemClickListener) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
            errorMsg = itemView.findViewById(R.id.error_msg);
            button = itemView.findViewById(R.id.retry_button);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listItemClickListener.onRetryClick(view, getAdapterPosition());
                }
            });
        }


        public void bindView(NetworkState networkState) {
            if (networkState != null && networkState.getStatus() == Status.RUNNING) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }

            if (networkState != null && networkState.getStatus() == Status.FAILED) {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(networkState.getMsg());
            } else {
                errorMsg.setVisibility(View.GONE);
            }
        }
    }
}
